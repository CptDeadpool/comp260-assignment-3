﻿using UnityEngine;
using System.Collections;

public class DepressionMove : MonoBehaviour {
    new Rigidbody rigidbody;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = true;
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void Destroy()
    {
        Destroy(gameObject);
    }
    void OnCollisionEnter(Collision collision) { 

        Destroy();

        //Debug.Log("Collision Enter: " + collision.gameObject.name);
    }
}
