﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Stats : MonoBehaviour {
    public Text speed;
    public Text score;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        PlayerMove player = FindObjectOfType<PlayerMove>();
        speed.text = "Speed: " + Mathf.Round(player.speed);
        score.text = "Score: " + Mathf.Round(player.score);
    }
}
