﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {
    new Rigidbody rigidbody;
    new AudioSource audio;
    public AudioClip sadcollide;
    public float force = 20f;
    public float speed = 20f;
    public LayerMask SadBubble;
    public LayerMask HappyBubble;
    public Transform startingPos;
    public int score = 0;
    public int highScore = 0;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        startingPos = rigidbody.transform;
        rigidbody.useGravity = false;
    }
    void FixedUpdate()
    {
        Vector3 dir = Vector3.zero;
        dir.x = Input.GetAxis("Horizontal");
        dir.y = Input.GetAxis("Vertical");

        rigidbody.velocity = dir * speed;
        

    }
    public void Missed()
    {
        if (speed >= 1)
        {
            speed=speed-0.5f;
        }
        else
        {
            speed = 0;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (SadBubble.Contains(collision.gameObject))
        {
            audio.PlayOneShot(sadcollide);
            if (speed >= 5)
            {
                speed = speed - 5f;
            } else
            {
                speed = 0;
            }
        }
        if (HappyBubble.Contains(collision.gameObject))
        {
            speed++;
            speed++;
            /*if (speed<=38)
            {
                speed++;
                speed++;
            } else if( speed==39)
            {
                speed++;
            }*/
            score++;
        }
    }
    public void Reset()
    {
        speed = 20;
        if (score > highScore)
        {
            highScore = score;
            score = 0;
        }
        else
        {
            score = 0;
        }
        rigidbody.MovePosition(startingPos.position);
        rigidbody.velocity = Vector3.zero;
        
    }

    // Update is called once per frame
    void Update () {
	
	}
}
