﻿using UnityEngine;
using System.Collections;

public class EndZone : MonoBehaviour {
    public AudioClip scoreClip;
    new AudioSource audio;
    private PlayerMove player;
    public LayerMask happy;

    // Use this for initialization
    void Start ()
    {
        player = FindObjectOfType<PlayerMove>();
        audio = GetComponent<AudioSource>();
    }
    void OnTriggerEnter(Collider collider)
    {
        //play score sound
        audio.PlayOneShot(scoreClip);
        if (happy.Contains(collider.gameObject))
        {
            player.Missed();

        }
        
    }
    // Update is called once per frame
    void Update ()
    {

    }
}
